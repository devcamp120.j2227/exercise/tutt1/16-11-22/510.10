//B1: import course model
const { default: mongoose } = require('mongoose');
const courseModel = require('../models/courseModel');

//B2: tạo các function crud

//get all courses
const getAllCourses = (request, response) => {
    console.log("Get all coureses");
    //B1: thu thập dữ liệu
    //B2: kiểm tra dữ liệu
    //B3: xử lý và trả về kết quả    
    courseModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                message:`internal server error: ${error.message}`
            });
        } else {
            return response.status(201).json({
                message:'Successfully load all data!',
                courses: data
            })
        }
    })
    
}

//get a course
const getCourseById = (request, response) => {    
    //B1: thu thập dữ liệu
    let id = request.params.courseid;  
    console.log(`Get All Courseid = ${id}`); 
    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return response.status(400).json({
            message:'Id is invalid'
        })
    }

    //B3: xử lý và trả về kết quả    
    courseModel.findById(id, (error, data) => {
        if (error) {
            return response.status(500).json({
                message:`internal server error: ${error.message}`
            });
        } else {
            return response.status(200).json({
                message:'Successfully load data by id!',
                course: data
            })
        }
    })
}

//create a course
const createCourse = (request, response) => {
     
    //B1: Thu thập dữ liệu
    let body = request.body;

    //B2: Kiểm tra dữ liệu
    if (!body.title) {
        return response.status(400).json({
            message:'title is required!'
        })
    }

    if (!Number.isInteger(body.noStudent) || body.noStudent < 0) {
        return response.status(400).json({
            message:'noStudent is invalid!'
        })
    }

    //B3: xử lý và trả về kết quả  
    console.log('create a course');
    console.log(body);    
    let newCourse = new courseModel({
        _id : mongoose.Types.ObjectId(),
        title : body.title,
        description : body.description,
        noStudent : body.noStudent
    });

    courseModel.create(newCourse, (error, data) => {
        if (error) {
            return response.status(500).json({
                message:`internal server error: ${error.message}`
            });
        } else {
            return response.status(201).json({
                message:'Successfully created!',
                course: data
            })
        }
    })
}

//update a course
const updateCourse = (request, response) => {

    //B1: Thu thập dữ liệu
    let id = request.params.courseid;
    let body = request.body;

    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return response.status(400).json({
            message:'Id is invalid'
        })
    }

    if (!body.title) {
        return response.status(400).json({
            message:'title is required!'
        })
    }

    if (!Number.isInteger(body.noStudent) || body.noStudent < 0) {
        return response.status(400).json({
            message:'noStudent is invalid!'
        })
    }
    //B3: Xử lý và trả về kết quả
    console.log('update a course');
    console.log({id, ...body});

    let newCourse = {
        title : body.title,
        description : body.description,
        noStudent : body.noStudent
    };

    courseModel.findByIdAndUpdate(id, newCourse, (error, data) => {
        if (error) {
            return response.status(500).json({
                message:`internal server error: ${error.message}`
            });
        } else {
            return response.status(201).json({
                message:'Successfully updated!',
                course: data
            })
        }
    })
    
    
}

//delete a course
const deleteCourse = (request, response) => {
    //B1: Thu thập dữ liệu
    let id = request.params.courseid;

    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return response.status(400).json({
            message:'Id is invalid'
        })
    }
    //B3: Xử lý và trả về kết quả
    console.log('delete a course' + id);
    courseModel.findByIdAndDelete(id, (error, data) => {
        if (error) {
            return response.status(500).json({
                message:`internal server error: ${error.message}`
            });
        } else {
            return response.status(204).json({
                message:'Successfully deleted!',
                course: data
            })
        }
    })
}

//B3: export thành module
module.exports = { getAllCourses, getCourseById, createCourse, updateCourse, deleteCourse }