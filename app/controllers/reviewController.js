const mongoose = require("mongoose");
const CourseModel = require("../models/courseModel");
const ReviewModel = require("../models/reviewModel");
const { updateCourse } = require("./courseController");

const createReviewOfCourse = (req, res) => {
    let courseId = req.params.courseId;
    let body = req.body;

    if(!mongoose.Types.ObjectId.isValid(courseId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "CourseId is invalid"
        });
    }
    if(!Number.isInteger(body.stars) && body.stars < 0) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "stars is invalid"
        });
    }

    let newReviews = {
        _id: mongoose.Types.ObjectId(),
        stars: body.stars,
        note: body.note
    }
    ReviewModel.create(newReviews, (error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            });
        } else {
            return CourseModel.findByIdAndUpdate(courseId, 
                {
                    $push: { reviews: data._id }
                },
                (error, updateCourse) => {
                    if(error) {
                        return res.status(500).json({
                            status: "Error 500: Internal server error",
                            message: error.message
                        })
                    } else {
                        return res.status(201).json({
                            status: "Successfully create reviews",
                            reviews: data
                        })
                    }
                }
            )
        }
    })
}

const getAllReviewOfCourse = (req, res) => {
    let courseId = req.params.courseId;

    if(!mongoose.Types.ObjectId.isValid(courseId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "CourseId is invalid"
        });
    }

    CourseModel.findById(courseId)
    .populate("reviews")
    .exec((error, data) => {
        if(error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(201).json({
                status: "Successfully get reviews of course",
                reviews: data
            })
        }
    })
}

const getReviewById = (req, res) => {
    //B1: thu thập dữ liệu
    let id = req.params.reviewsId;
    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            message:'Id is invalid'
        })
    }
    //B3: xử lý và trả về kết quả    
    ReviewModel.findById(id, (error, data) => {
        if (error) {
            return res.status(500).json({
                message:`internal server error: ${error.message}`
            });
        } else {
            return res.status(200).json({
                message:'Successfully load data by id!',
                course: data
            })
        }
    })
}

const updateReviewById = (req, res) => {
    //B1: Thu thập dữ liệu
    let id = req.params.reviewsId;
    let body = req.body;
    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            message:'Id is invalid'
        })
    }
    if(!Number.isInteger(body.stars) && body.stars < 0) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "stars is invalid"
        });
    }
    //B3: Xử lý và trả về kết quả
    let newReviews = {
        stars: body.stars,
        note: body.note
    }
    ReviewModel.findByIdAndUpdate(id, newReviews, (error, data) => {
        if (error) {
            return res.status(500).json({
                message:`internal server error: ${error.message}`
            });
        } else {
            return res.status(201).json({
                message:'Successfully updated!',
                reviews: data
            })
        }
    })   
}

const deleteReviewById = (request, response) => {
    //B1: Thu thập dữ liệu
    let id = request.params.reviewsId;
    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return response.status(400).json({
            message:'Id is invalid'
        })
    }
    //B3: Xử lý và trả về kết quả
    ReviewModel.findByIdAndDelete(id, (error, data) => {
        if (error) {
            return response.status(500).json({
                message:`internal server error: ${error.message}`
            });
        } else {
            return response.status(204).json({
                message:'Successfully deleted!',
                course: data
            })
        }
    })
}

module.exports = { createReviewOfCourse, getAllReviewOfCourse, getReviewById, updateReviewById, deleteReviewById }