//khai báo thư viện express
const express = require('express');
const { createReviewOfCourse, getAllReviewOfCourse, getReviewById, updateReviewById, deleteReviewById } = require('../controllers/reviewController');
const reviewMiddleware = require('../middlewares/reviewMiddleware');

//tạo router
const reviewRouter = express.Router();
//sủ dụng middle ware
reviewRouter.use(reviewMiddleware);


reviewRouter.get('/courses/:courseId/reviews', getAllReviewOfCourse);
reviewRouter.get('/reviews/:reviewsId', getReviewById);
reviewRouter.post('/courses/:courseId/reviews', createReviewOfCourse);
reviewRouter.put('/reviews/:reviewsId', updateReviewById);
reviewRouter.delete('/reviews/:reviewsId', deleteReviewById);

module.exports = { reviewRouter };